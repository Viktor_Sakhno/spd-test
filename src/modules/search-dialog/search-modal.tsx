import React, {FC} from "react";
import {
    mergeStyleSets,
    Modal,
    initializeIcons,
} from 'office-ui-fabric-react';

import {SearchDialog} from './search-dialog-controller';


initializeIcons();

type Props = {
    isOpen: boolean,
    hideModal: () => void
}

export const SearchModal: FC<Props> = ({isOpen, hideModal}) => {
    return (
        <Modal
            isOpen={isOpen}
            isBlocking={false}
            containerClassName={contentStyles.container}
        >
            <SearchDialog hideModal={hideModal}/>
        </Modal>
    );
}

const contentStyles = mergeStyleSets({
    container: {
        display: 'flex',
        flexFlow: 'column nowrap',
        alignItems: 'stretch',
        border: `1px solid #000`,
        minWidth: `800px`,
        height: '100vh'
    }
});



