export type TableData = IndexType | PeopleType | CompanyType;

export type PeopleType = {
    country: string,
    name: string,
    'billion USD': number,
    rank: number,
    year: number
}
export type IndexType = {
    index: string,
    'index growth': number,
    ticker: string,
    year: number
}

export type CompanyType = {
    country: string,
    company: string,
    'capitalization billion USD': number,
    rank: number,
    year: number
}

export type ListType = {
    key: string,
    text: string
};

