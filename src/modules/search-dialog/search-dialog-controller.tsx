import React, {FC, useEffect, useState} from 'react';
import {DataList} from './components/datalist';
import {Search} from './components/search';
import {DropdownList} from './components/dropdown';
import {SelectSearch} from './components/select-search';
import {ListType, TableData} from './typedefs';
import {DefaultButton} from 'office-ui-fabric-react';
import {ScrollablePane, ScrollbarVisibility} from 'office-ui-fabric-react/lib/ScrollablePane';
import {Sticky, StickyPositionType} from 'office-ui-fabric-react/lib/Sticky';
import {SearchDialogHeader} from './components/search-dialog-header';
import {SearchDialogFooter} from './components/search-dialog-footer';
import {getUniqueCollection} from './utils/get-uniqe-colection';
import {getFilteredData} from './utils/get-filtered-data';
import {getCollectionAndData} from './utils/get-colection-and-data';
import {getField} from './utils/get-field';
import {getList} from './utils/get-list';

type Props = {
    hideModal: () => void
};

export const SearchDialog: FC<Props> = ({hideModal}) => {
    const [data, setData] = useState<TableData[]>([]);
    const [filteredData, setFilteredData] = useState<TableData[]>([]);
    const [newFilteredData, setNewFilteredData] = useState<TableData[] | undefined>(undefined);
    const [selectedElem, setSelectedElem] = useState<ListType>({key: 'all', text: 'All'});
    const [firstFilterValue, setFirstFilterValue] = useState<string>('All');
    const [secondFilterValue, setSecondFilterValue] = useState<string>('All');
    const [firstList, setFirstList] = useState<ListType[]>([]);
    const [secondList, setSecondList] = useState<ListType[]>([]);
    const [keys, setKeys] = useState<string[]>([]);
    const [searchValue, setSearchValue] = useState('');
    const [firstFilterField, setFirstFilterField] = useState('country');
    const [firstTitle, setFirstTitle] = useState('Country');
    const [secondFilterField, setSecondFilterField] = useState('year');
    const [secondTitle, setSecondTitle] = useState('Year');
    const [searchFor, setSearchFor] = useState('people');

    const firstFilter = (value: string) => {
        let newList = [{key: 'all', text: 'All'}];
        setFirstFilterValue(value);

        if (value === 'All' && secondFilterValue === 'All') {
            setSecondList(newList.concat(getUniqueCollection(filteredData, secondFilterField)));
            setNewFilteredData(filteredData);

        } else if (value && secondFilterValue === 'All') {
            const filtered = getFilteredData(filteredData, firstFilterField, value);
            setSecondList(newList.concat(getUniqueCollection(filtered, secondFilterField)));
            setNewFilteredData(filtered);

        } else if (value === 'All' && secondFilterValue !== 'All') {
            const {filtered, list} = getCollectionAndData(filteredData, secondFilterField, secondFilterValue);
            setSecondList(newList.concat(list));
            setNewFilteredData(filtered);

        } else if (value && secondFilterValue !== 'All') {
            let colection = new Set();
            const filtered = filteredData.filter(item => {
                const firstField = getField(item, firstFilterField);
                const secondField = getField(item, secondFilterField);
                firstField.toLowerCase().includes(value.toLowerCase()) && colection.add(String(item[secondFilterField as keyof TableData]));
                return firstField.toLowerCase().includes(value.toLowerCase()) && secondField.toLowerCase().includes(secondFilterValue.toLowerCase());
            });
            const secondList = getList(colection);
            setSecondList(newList.concat(secondList));
            setNewFilteredData(filtered);
        }
    }

    const secondFilter = (value: string) => {
        let newList = [{key: 'all', text: 'All'}];
        setSecondFilterValue(value);

        if (value === 'All' && firstFilterValue === 'All') {
            setFirstList(newList.concat(getUniqueCollection(filteredData, firstFilterField)));
            setNewFilteredData(filteredData);

        } else if (value && firstFilterValue === 'All') {
            const filtered = getFilteredData(filteredData, secondFilterField, value);
            setFirstList(newList.concat(getUniqueCollection(filtered, firstFilterField)));
            setNewFilteredData(filtered);

        } else if (value === 'All' && firstFilterValue !== 'All') {
            const {filtered, list} = getCollectionAndData(filteredData, firstFilterField, firstFilterValue);
            setFirstList(newList.concat(list));
            setNewFilteredData(filtered);

        } else if (value && firstFilterValue !== 'All') {
            let colection = new Set();
            const filtered = filteredData.filter(item => {
                const firstField = getField(item, firstFilterField);
                const secondField = getField(item, secondFilterField);
                secondField.toLowerCase().includes(value.toLowerCase()) && colection.add(String(item[firstFilterField as keyof TableData]));
                return firstField.toLowerCase().includes(firstFilterValue.toLowerCase()) && secondField.toLowerCase().includes(value.toLowerCase());
            });
            const firstList = getList(colection);
            setFirstList(newList.concat(firstList));
            setNewFilteredData(filtered);
        }
    }

    const getKeys = (dataList: object[]) => {
        let titlesObject = new Set();
        dataList.forEach(obj => {
            Object.keys(obj).forEach(prop => titlesObject.add(prop));
        });
        setKeys(Array.from(titlesObject) as string[]);
    }

    const refreshList = (newData: TableData[]) => {
        let newList = [{key: 'all', text: 'All'}];
        let firstSet = new Set();
        let secondSet = new Set();
        newData.forEach(item => {
            const firstField = getField(item, firstFilterField);
            const secondField = getField(item, secondFilterField);
            firstSet.add(firstField);
            secondSet.add(secondField);
        });
        const firstArr = getList(firstSet);
        const secondArr = getList(secondSet);
        setFirstList(newList.concat(firstArr));
        setSecondList(newList.concat(secondArr));
    }

    const filterData = (newValue: string | undefined) => {
        setSearchValue(newValue ? newValue : '');
        if (keys.length !== 0 && newValue) {
            const value = newValue.toLowerCase();
            const dataFiltered = data.filter(item => {
                let result = false;
                const length = keys.length;
                for (let i = 0; i < length; i++) {
                    const str = String(item[keys[i] as keyof TableData]);
                    if (str.toLowerCase().includes(value)) {
                        result = true;
                        break;
                    }
                }
                return result;
            });
            setNewFilteredData(dataFiltered);
            setFilteredData(dataFiltered);
            setSelectedElem({key: 'all', text: 'All'});
        } else {
            setNewFilteredData(data);
            setFilteredData(data);
            setSelectedElem({key: 'all', text: 'All'});
        }
    }

    useEffect(() => {
        fetch(`/mock-data/data.json`)
            .then(result => result.json())
            .then(data => {
                setData(data[searchFor])
            })
            .catch(err => {
                console.log(err);
            });
    }, [searchFor]);

    useEffect(() => {
        getKeys(data);
        refreshList(data);
        setFilteredData(data);
        setNewFilteredData(data);
        setSelectedElem({key: 'all', text: 'All'});
    }, [data]);

    useEffect(() => {
        refreshList(filteredData);
    }, [filteredData]);

    return (
        <div>
            <ScrollablePane scrollbarVisibility={ScrollbarVisibility.auto}>
                <Sticky stickyPosition={StickyPositionType.Header}>
                    <SearchDialogHeader hideModal={hideModal}/>
                    <div style={{display: 'flex', justifyContent: 'space-between', padding: '10px 30px'}}>
                        <Search onSearch={filterData} value={searchValue}/>
                        <DefaultButton text="Search"/>
                        <SelectSearch
                            setSearchFor={setSearchFor}
                            setFirstFilterField={setFirstFilterField}
                            setFirstTitle={setFirstTitle}
                            setSecondFilterField={setSecondFilterField}
                            setSecondTitle={setSecondTitle}
                            setFirstFilterValue={setFirstFilterValue}
                            setSecondFilterValue={setSecondFilterValue}
                        />
                    </div>
                    <div style={{display: 'flex', justifyContent: 'flex-start', padding: '0 10px'}}>
                        <DropdownList selectedElem={selectedElem} filter={firstFilter} title={firstTitle}
                                      list={firstList}/>
                        <DropdownList selectedElem={selectedElem} filter={secondFilter} title={secondTitle}
                                      list={secondList}/>

                    </div>
                </Sticky>
                <DataList data={newFilteredData || filteredData} keys={keys}/>
                <Sticky stickyPosition={StickyPositionType.Footer}>
                    <SearchDialogFooter/>
                </Sticky>
            </ScrollablePane>
        </div>
    );
}

