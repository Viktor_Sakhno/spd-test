export const getList = (collection: any) => {
    return (Array.from(collection) as string[]).map(elem => ({key: elem.toLowerCase(), text: elem}))
}