import {TableData} from '../typedefs';

export const getField = (item: TableData, field: string) => {
    return String(item[field as keyof TableData]);
}