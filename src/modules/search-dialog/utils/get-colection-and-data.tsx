import {TableData} from '../typedefs';

export const getCollectionAndData = (data: TableData[], filterField: string, value: string) => {
    let collection = new Set();
    const filtered = data.filter(item => {
        collection.add(String(item[filterField as keyof TableData]));
        const field = String(item[filterField as keyof TableData]);
        return field.toLowerCase().includes(value.toLowerCase());
    });
    const list = (Array.from(collection) as string[]).map(elem => ({key: elem.toLowerCase(), text: elem}));
    return {filtered, list};
}