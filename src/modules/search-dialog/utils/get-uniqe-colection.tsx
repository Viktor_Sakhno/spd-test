import {TableData} from '../typedefs';

export const getUniqueCollection = (data: TableData[], field: string) => {
    let collection = new Set();
    data.forEach(item => {
        const secondField = String(item[field as keyof TableData]);
        collection.add(secondField);
    });
    return (Array.from(collection) as string[]).map(elem => ({key: elem.toLowerCase(), text: elem}));
}