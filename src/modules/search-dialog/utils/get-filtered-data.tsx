import {TableData} from '../typedefs';

export const getFilteredData = (data: TableData[], filterField: string, value: string) => {
    return data.filter(item => {
        const field = String(item[filterField as keyof TableData]);
        return field.toLowerCase().includes(value.toLowerCase());
    });
}