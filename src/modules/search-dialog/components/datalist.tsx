import * as React from 'react';
import {Fabric} from 'office-ui-fabric-react/lib/Fabric';
import {
    DetailsList,
    DetailsListLayoutMode,
    SelectionMode,
    IColumn, IDetailsHeaderProps, IDetailsColumnRenderTooltipProps,
} from 'office-ui-fabric-react/lib/DetailsList';
import {initializeIcons} from 'office-ui-fabric-react/lib/Icons';
import {TableData} from '../typedefs';
import {IRenderFunction} from 'office-ui-fabric-react/lib/Utilities';
import {TooltipHost} from 'office-ui-fabric-react/lib/Tooltip';
import {Sticky, StickyPositionType} from 'office-ui-fabric-react/lib/Sticky';

initializeIcons();

export interface IDataList {
    columns: IColumn[];
    items: TableData[];
    isModalSelection: boolean;
    currentColumn: undefined | IColumn;
}

type Props = {
    data: TableData[],
    keys: string[]
}

const onRenderDetailsHeader: IRenderFunction<IDetailsHeaderProps> = (props, defaultRender) => {
    if (!props) {
        return null;
    }
    const onRenderColumnHeaderTooltip: IRenderFunction<IDetailsColumnRenderTooltipProps> = tooltipHostProps => (
        <TooltipHost {...tooltipHostProps} />
    );
    return (
        <Sticky stickyPosition={StickyPositionType.Header} isScrollSynced>
            {defaultRender!({
                ...props,
                onRenderColumnHeaderTooltip,
            })}
        </Sticky>
    );
};

export class DataList extends React.Component<Props, IDataList> {
    constructor(props: Props) {
        super(props);

        const columns: IColumn[] = props.keys.map((item, idx) => this._createItemHeader(item, idx));

        this.state = {
            items: _copyAndSort(props.data, 'name', false),
            columns: columns,
            isModalSelection: true,
            currentColumn: undefined
        };
    }

    componentDidUpdate(nextProps: Props) {
        const {currentColumn} = this.state;
        const {data, keys} = this.props;
        if (nextProps.data !== data) {
            if (data) {
                this.setState({items: _copyAndSort(data, currentColumn?.fieldName || 'name', currentColumn?.isSortedDescending)})
            }
        }
        if (nextProps.keys !== keys) {
            if (keys) {
                this.setState({columns: keys.map((item, idx) => this._createItemHeader(item, idx))})
            }
        }
    }

    public render() {
        const {columns, items} = this.state;

        return (
            <Fabric>
                <DetailsList
                    items={items}
                    columns={columns}
                    selectionMode={SelectionMode.multiple}
                    getKey={this._getKey}
                    setKey="multiple"
                    layoutMode={DetailsListLayoutMode.justified}
                    isHeaderVisible={true}
                    selectionPreservedOnEmptyClick={true}
                    enterModalSelectionOnTouch={true}
                    ariaLabelForSelectionColumn="Toggle selection"
                    ariaLabelForSelectAllCheckbox="Toggle selection for all items"
                    checkButtonAriaLabel="Row checkbox"
                    onRenderDetailsHeader={onRenderDetailsHeader}
                />
            </Fabric>
        );
    }

    private _createItemHeader(item: string, idx: number): IColumn {
        if (idx === 0) {
            return (
                {
                    key: `column${idx}`,
                    name: this._changeLetter(item),
                    fieldName: item,
                    minWidth: 70,
                    maxWidth: 150,
                    isResizable: true,
                    isSorted: true,
                    isSortedDescending: false,
                    isCollapsible: true,
                    onColumnClick: this._onColumnClick,
                    isPadded: true
                }
            );
        } else {
            return (
                {
                    key: `column${idx}`,
                    name: this._changeLetter(item),
                    fieldName: item,
                    minWidth: 70,
                    maxWidth: 150,
                    isResizable: true,
                    isCollapsible: true,
                    onColumnClick: this._onColumnClick,
                    isPadded: true
                }
            );
        }
    }

    private _changeLetter(item: string) {
        return item.charAt(0).toUpperCase() + item.slice(1);
    }

    private _getKey(item: any, index?: number): string {
        return item.key;
    }

    private _onColumnClick = (ev: React.MouseEvent<HTMLElement>, column: IColumn): void => {
        const {columns, items} = this.state;
        const newColumns: IColumn[] = columns.slice();
        const currColumn: IColumn = newColumns.filter(currCol => column.key === currCol.key)[0];
        newColumns.forEach((newCol: IColumn) => {
            if (newCol === currColumn) {
                currColumn.isSortedDescending = !currColumn.isSortedDescending;
                currColumn.isSorted = true;
            } else {
                newCol.isSorted = false;
                newCol.isSortedDescending = true;
            }
        });
        const newItems = _copyAndSort(items, currColumn.fieldName!, currColumn.isSortedDescending);
        this.setState({
            currentColumn: currColumn,
            columns: newColumns,
            items: newItems,
        });
    };
}

function _copyAndSort<T>(items: T[], columnKey: string, isSortedDescending?: boolean): T[] {
    const key = columnKey as keyof T;
    return items.slice(0).sort((a: T, b: T) => ((isSortedDescending ? a[key] < b[key] : a[key] > b[key]) ? 1 : -1));
}







