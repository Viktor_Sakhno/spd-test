import React, {FC} from 'react';
import {DefaultButton, mergeStyleSets, PrimaryButton} from 'office-ui-fabric-react';

type Props = {};
export const SearchDialogFooter: FC<Props> = props => {
    return (
        <div className={contentStyles.footer}>
            <DefaultButton text="Cancel"/>
            <PrimaryButton className={contentStyles.button} text="Add"/>
        </div>
    );
}

const contentStyles = mergeStyleSets({
    footer: {
        flex: '1 1 auto',
        display: 'flex',
        justifyContent: 'flex-end',
        borderTop: `1px solid #e4e4e4`,
        alignItems: 'center',
        padding: '22px 30px 22px 30px',
    },
    button: {
        marginLeft: '10px'
    }
});
