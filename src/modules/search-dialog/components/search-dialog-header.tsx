import React, {FC} from 'react';
import {FontWeights, getTheme, IconButton, mergeStyleSets} from 'office-ui-fabric-react';

type Props = {
    hideModal: () => void
};

export const SearchDialogHeader: FC<Props> = ({hideModal}) => {
    return (
        <div className={contentStyles.header}>
            <span>Search</span>
            <IconButton
                styles={iconButtonStyles}
                iconProps={cancelIcon}
                ariaLabel="Close popup modal"
                onClick={hideModal}
            />
        </div>
    );
}

const theme = getTheme();
const contentStyles = mergeStyleSets({
    header: [
        theme.fonts.xLargePlus,
        {
            flex: '1 1 auto',
            borderBottom: `1px solid #e4e4e4`,
            color: theme.palette.neutralPrimary,
            display: 'flex',
            alignItems: 'center',
            fontWeight: FontWeights.semibold,
            padding: '12px 30px 14px 30px',
        },
    ]
});

const iconButtonStyles = {
    root: {
        color: theme.palette.neutralPrimary,
        marginLeft: 'auto',
        marginTop: '4px',
        marginRight: '2px',
    },
    rootHovered: {
        color: theme.palette.neutralDark,
    },
};

const cancelIcon = {iconName: 'Cancel'};