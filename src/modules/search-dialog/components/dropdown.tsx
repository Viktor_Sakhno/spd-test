import React, {FC, useEffect} from 'react';
import {Dropdown, IDropdownOption, IDropdownStyles} from 'office-ui-fabric-react/lib/Dropdown';
import {ListType} from '../typedefs';
import {mergeStyleSets} from 'office-ui-fabric-react';


type Props = {
    filter: (value: string) => void,
    title: string,
    list: ListType[],
    selectedElem: ListType
}

export const DropdownList: FC<Props> = ({filter, title, list, selectedElem}) => {
    const [selectedItem, setSelectedItem] = React.useState<IDropdownOption>();

    const onChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
        setSelectedItem(item);
        filter(item.text);
    };

    useEffect(() => {
        setSelectedItem(selectedElem);
    }, [selectedElem]);

    return (
        <Dropdown
            label={title}
            selectedKey={selectedItem ? selectedItem.key : 'all'}
            // @ts-ignore
            onChange={onChange}
            placeholder="Select an option"
            options={list}
            styles={dropdownStyles}
            className={contentStyles.dropdown}
        />
    );
};

const contentStyles = mergeStyleSets({
    dropdown: {
        width: '40%'
    }
});

const dropdownStyles: Partial<IDropdownStyles> = {
    root: {
        display: 'flex',
        flexFlow: 'row',
    },
    label: {
        flex: '1',
        padding: '0 5px',
        lineHeight: '32px',
        textAlign: 'center'
    },
    dropdown: {
        flex: '2 0 auto'
    }
};