import React, {FC} from 'react';
import {SearchBox} from 'office-ui-fabric-react/lib/SearchBox';
import {initializeIcons} from 'office-ui-fabric-react/lib/Icons';
import {mergeStyleSets} from 'office-ui-fabric-react';


initializeIcons();

type Props = {
    onSearch: (value: string | undefined) => void,
    value: string
};
export const Search: FC<Props> = ({onSearch, value}) => {
    const handler = (event?: React.ChangeEvent<HTMLInputElement>, newValue?: string) => {
        onSearch(newValue);
    }
    return (
        <SearchBox
            placeholder="Search"
            underlined={true}
            onChange={handler}
            onClear={handler}
            onEscape={handler}
            value={value}
            className={contentStyles.searchBox}
        />
    );
}

const contentStyles = mergeStyleSets({
    searchBox: {
        flex: '1',
        marginRight: "15px"
    }
});