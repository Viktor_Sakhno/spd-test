import React, {FC} from 'react';
import {Dropdown, IDropdownOption, IDropdownStyles} from 'office-ui-fabric-react/lib/Dropdown';
import {mergeStyleSets} from 'office-ui-fabric-react';


type Props = {
    setSearchFor: (value: string) => void,
    setFirstFilterField: (field: string) => void,
    setFirstTitle: (title: string) => void,
    setSecondFilterField: (field: string) => void,
    setSecondTitle: (title: string) => void,
    setFirstFilterValue: (value: string) => void,
    setSecondFilterValue: (value: string) => void
}

const list = [
    {key: 'people', text: 'People'},
    {key: 'index', text: 'Index'},
    {key: 'company', text: 'Company'}
];

export const SelectSearch: FC<Props> =
    ({setFirstFilterField, setFirstTitle, setSecondFilterField, setSecondTitle, setSearchFor, setFirstFilterValue, setSecondFilterValue}) => {
        const [selectedItem, setSelectedItem] = React.useState<IDropdownOption>({key: 'people', text: 'People'});

        const onChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
            setSelectedItem(item);
            setFirstFilterValue('All');
            setSecondFilterValue('All');
            if (item.key === 'people') {
                setSearchFor('people');
                setFirstFilterField('country');
                setFirstTitle('Country');
                setSecondFilterField('year');
                setSecondTitle('Year');
            } else if (item.key === 'index') {
                setSearchFor('index');
                setFirstFilterField('index');
                setFirstTitle('Index');
                setSecondFilterField('year');
                setSecondTitle('Year');
            } else if (item.key === 'company') {
                setSearchFor('company');
                setFirstFilterField('company');
                setFirstTitle('Company');
                setSecondFilterField('year');
                setSecondTitle('Year');
            }
        };

        return (
            <Dropdown
                label="Search for"
                selectedKey={selectedItem ? selectedItem.key : 'all'}
                // @ts-ignore
                onChange={onChange}
                placeholder="Select an option"
                options={list}
                styles={dropdownStyles}
                className={contentStyles.dropdown}
            />
        );
    };


const contentStyles = mergeStyleSets({
    dropdown: {
        width: '40%'
    }
});

const dropdownStyles: Partial<IDropdownStyles> = {
    root: {
        display: 'flex',
        flexFlow: 'row',
    },
    label: {
        flex: '1',
        padding: '0 20px',
        lineHeight: '32px',
        textAlign: 'center',
        whiteSpace: 'nowrap'
    },
    dropdown: {
        flex: '2 0 auto'
    }
};