import React, {useState} from "react";
import "./App.css";
import {DefaultButton} from "@fluentui/react";
import {SearchModal} from './modules/search-dialog/search-modal';




export default function App() {
    const [isModalOpen, setModalOpen] = useState(false);
    return (
        <div className="App">
            <DefaultButton
                style={{marginTop: '40px'}}
                onClick={() => setModalOpen(true)}
                text="Open Search"
            />
            <SearchModal
                isOpen={isModalOpen}
                hideModal={() => setModalOpen(false)}
            />
        </div>
    );
}